import java.util.Objects;

class MyHashMap<K, T>
{
    private int defaultCapacity = 4;
    Node<K,T>[] buckets = null;

    public MyHashMap() 
    {
        buckets = new Node[defaultCapacity];
    }

    private int hash(Object key) 
    {
	return key.hashCode() % (defaultCapacity-1);
    }
	
    public void put(K key, T value) 
    {
	int hashcode = hash(key);

        if (buckets[hashcode] == null) 
	{
            Node head = new Node(key, value, null);
            buckets[hashcode] = head;
        } 
	else 
	{
            Node<K,T> temp = buckets[hashcode];
            while (temp.next != null) 
	    {
                temp = temp.next;
            }
            Node<K,T> newNode = new Node<K,T>(key, value, null);
            newNode.next = temp;
            buckets[hashcode] = newNode;

        }
    }
	
    public T get(K key) 
    {
        int hashcode = hash(key);
        if (buckets[hashcode] == null) 
	{
            return null;
        } 
	else 
	{
            Node<K,T> temp = buckets[hashcode];
            while (temp != null) 
	    {
		if (temp.key.equals(key)) 
		{
                    return (T) temp.value;
                }
                temp = temp.next;
            }
            return null;
        }
    }
	
    public void delete(K key) 
    {
        int hashcode = hash(key);
        Node<K,T> deleteNode = buckets[hashcode];
        deleteNode = null;
        buckets[hashcode] = deleteNode;
    }
}