//Using Java Generic Type Naming convention where K stands for Type for Key and T for Type for Value
class Node<K, T> 
{
    public K key;
    public T value;
    public Node next;

    public Node(K key, T value, Node next) 
    {
	this.key = key;
        this.value = value;
        this.next = next;
    }
}