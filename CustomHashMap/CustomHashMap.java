import java.io.*;

class CustomHashMap 
{
     public static void main(String[] args) 
     {
           MyHashMap<String, Integer> NameNumberPair = new MyHashMap<String, Integer>();

	   System.out.println("Key-value pairs are: JAMES-1000,KING-12,CLARK-121,JANE-32,KALLI-98,BRENDA-2");

	   NameNumberPair.put("JAMES",1000);
           NameNumberPair.put("KING", 12);
           NameNumberPair.put("CLARK", 121);
	   NameNumberPair.put("JANE", 32);
	   NameNumberPair.put("KALLI", 98);
	   NameNumberPair.put("BRENDA", 2);
	   
           System.out.println("value corresponding to key KING=" + NameNumberPair.get("KING"));
	   System.out.println("value corresponding to key JANE=" + NameNumberPair.get("JANE"));
	   System.out.println("value corresponding to key BRENDA=" + NameNumberPair.get("BRENDA"));
    }
}