import java.util.*;
class Football
{
    public static void EvaluationBlock()
    {
        Scanner scanner = new Scanner(System.in);   
        int NoOfTestCases = scanner.nextInt();
        int resultArray[] = new int[NoOfTestCases];
        
        for(int counter1 = 0;counter1 < NoOfTestCases;counter1++)
       	{
            int NumberOfPasses = scanner.nextInt();
            int initialPlayerId = scanner.nextInt();
	    scanner.nextLine();         
	    
	    int PastPass = initialPlayerId, RecentPass =-1;
           
 	    for(int counter2 = 0;counter2 < NumberOfPasses;counter2++)
            {
            	String input = scanner.nextLine();
		char typeOfPass = input.charAt(0);
				
		if(typeOfPass==('P'))
                {
                    String[] array = input.split(" ");
                    PastPass = RecentPass;
		    RecentPass = Integer.parseInt(array[1]);              
 		}
                
		else if(typeOfPass==('B'))
                {
                    int temp = RecentPass;
                    RecentPass = PastPass;
                    PastPass = temp;
                }
	     }
	     resultArray[counter1] = RecentPass;
        }
        
	for(int counter = 0;counter < NoOfTestCases;counter++)
	{
            System.out.println("Player "+resultArray[counter]);
        }
    }
     
	public static void main(String args[] ) throws Exception 
	{
            EvaluationBlock();
	}
}