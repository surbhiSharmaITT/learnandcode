import java.util.*;
import java.io.*;
import java.util.Objects;

class Node<K, T> 
{
    public K key;
    public T value;
    public Node next;

    public Node(K key, T value, Node next) 
    {
	this.key = key;
        this.value = value;
        this.next = next;
    }
}

class MyHashMap<K, T>
{
    private int defaultCapacity = 4;
    Node<K,T>[] buckets = null;

    public MyHashMap() 
    {
        buckets = new Node[defaultCapacity];
    }

    private int hash(Object key) 
    {
	return key.hashCode() % (defaultCapacity-1);
    }
	
    public void put(K key, T value) 
    {
	int hashcode = hash(key);

        if (buckets[hashcode] == null) 
	{
            Node head = new Node(key, value, null);
            buckets[hashcode] = head;
        } 
	else 
	{
            Node<K,T> temp = buckets[hashcode];
            while (temp.next != null) 
	    {
                temp = temp.next;
            }
            Node<K,T> newNode = new Node<K,T>(key, value, null);
            newNode.next = temp;
            buckets[hashcode] = newNode;

        }
    }
	
    public T get(K key) 
    {
        int hashcode = hash(key);
        if (buckets[hashcode] == null) 
	{
            return null;
        } 
	else 
	{
            Node<K,T> temp = buckets[hashcode];
            while (temp != null) 
	    {
		if (temp.key.equals(key)) 
		{
                    return (T) temp.value;
                }
                temp = temp.next;
            }
            return null;
        }
    }
}

class HelpMaxUsingHashMap 
{
   	MyHashMap <String,String> mapOfPersonNameAndTheirGame = new MyHashMap<>();
    	MyHashMap <String, Integer> mapOfGameAndItsPopularity = new MyHashMap<>();
	int maximumPopularity = 1;
	String gameWithMaximumPopularity = "";

	public void SurveyOfMaximumPopularGameByMax() throws IOException
	{
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));  		
		int numberOfEntries = Integer.parseInt(bufferedReader.readLine());
		if(IsValidNumberOfEntry(numberOfEntries))
		{
	    		for(int inputEntry=0; inputEntry<numberOfEntries; inputEntry++)
	    		{
    	   			String inputLine = bufferedReader.readLine();
				String[] nameSportInput = inputLine.split(" ");
				if(IsValidEntryOnMap(nameSportInput[0] , nameSportInput[1]))
				{
    	   				if(mapOfPersonNameAndTheirGame.get(nameSportInput[0])== null)
					{
    	       					mapOfPersonNameAndTheirGame.put(nameSportInput[0],nameSportInput[1]);
						AppendingInMapOfGameAndItsPopularity(nameSportInput[1]);
					}
				}
			}
	    	}
	}

	public boolean IsValidNumberOfEntry(int numberOfEntries)
	{
		if(numberOfEntries >= 1 && numberOfEntries <= 100000)
			return true;
		else
			return false;
 	}

	public boolean IsValidEntryOnMap(String personName, String gameName)
	{
		if((personName.length() >= 1 && personName.length() <= 10) && (gameName.length() >= 1 && gameName.length() <= 10))
			return true;
		else
			return false;
 	}

	public void AppendingInMapOfGameAndItsPopularity(String sport)
	{
		if(mapOfGameAndItsPopularity.get(sport)==null)
    	    		mapOfGameAndItsPopularity.put(sport,1);
		else
		{
			int currentGamePopularity = mapOfGameAndItsPopularity.get(sport)+1;
    	    		mapOfGameAndItsPopularity.put(sport, currentGamePopularity);
			checkIsCurrentGameMostPopular(sport, currentGamePopularity);
		}
	}
	
	public void checkIsCurrentGameMostPopular(String sport, int currentGamePopularity)
	{
		if(currentGamePopularity >= maximumPopularity)
		{
                 	maximumPopularity = currentGamePopularity;
                	gameWithMaximumPopularity = sport;
            	}
	}

	public void PrintFavoriteGame()
	{
		System.out.println(gameWithMaximumPopularity);
		//Evaluating the popularity of sports "Football"
    		if(mapOfGameAndItsPopularity.get("football")==null)
    	   		System.out.println(mapOfGameAndItsPopularity.get("football"));
    		else
    	   		System.out.println("0");
	}
}
class MyStatistics
{
	public static void main (String args[]) throws IOException
	{
		HelpMaxUsingHashMap helpMaxUsingHashMapObject = new HelpMaxUsingHashMap();

        	helpMaxUsingHashMapObject.SurveyOfMaximumPopularGameByMax();
		helpMaxUsingHashMapObject.PrintFavoriteGame();
    	}
}