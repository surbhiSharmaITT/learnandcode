import org.junit.Assert;
import org.junit.Test;


public class AddAlternateElementsTest {
	
	@Test
	public void AddOddElements_NullMatrix_Zero(){
		//Arrange
		 IMatrix matrix = new Matrix();
		 int inputArray[] = {0,0,0,0,0,0,0,0,0};
		 matrix.setMatrix(inputArray);
		 
		//Act
		 int output = matrix.AddOddElements();
		 
		//Assert
		 Assert.assertEquals(output,0);
		 
	}

	@Test
	public void AddEvenElements_NullMatrix_Zero(){
		//Arrange
		 IMatrix matrix = new Matrix();
		 int inputArray[] = {0,0,0,0,0,0,0,0,0};
		 matrix.setMatrix(inputArray);
		 
		//Act
		 int output = matrix.AddEvenElements();
		 
		//Assert
		 Assert.assertEquals(output,0);
		 
	}
	
	@Test
	public void AddOddElements_IdentityMatrix_Three(){
		//Arrange
		 IMatrix matrix = new Matrix();
		 int inputArray[] = {1,0,0,0,1,0,0,0,1};
		 matrix.setMatrix(inputArray);
		 
		//Act
		 int output = matrix.AddOddElements();
		 
		//Assert
		 Assert.assertEquals(output,3);
		 
	}

	@Test
	public void AddEvenElements_IdentityMatrix_Zero(){
		//Arrange
		 IMatrix matrix = new Matrix();
		 int inputArray[] = {1,0,0,0,1,0,0,0,1};
		 matrix.setMatrix(inputArray);
		 
		//Act
		 int output = matrix.AddEvenElements();
		 
		//Assert
		 Assert.assertEquals(output,0);
		 
	}
	
	@Test
	public void AddOddElements_RandomMatrix1(){
		//Arrange
		 IMatrix matrix = new Matrix();
		 int inputArray[] = {24,111,2,15,119,1,26,70,420};
		 matrix.setMatrix(inputArray);
		 
		//Act
		 int output = matrix.AddOddElements();
		 
		//Assert
		 Assert.assertEquals(output,591);
		 
	}

	@Test
	public void AddEvenElements_RandomMatrix1(){
		//Arrange
		 IMatrix matrix = new Matrix();
		 int inputArray[] = {24,111,2,15,119,1,26,70,420};
		 matrix.setMatrix(inputArray);
		 
		//Act
		 int output = matrix.AddEvenElements();
		 
		//Assert
		 Assert.assertEquals(output,197);
		 
	}

	@Test
	public void AddOddElements_RandomMatrix2(){
		//Arrange
		 IMatrix matrix = new Matrix();
		 int inputArray[] = {5,4,6,8,1,9,0,23,2};
		 matrix.setMatrix(inputArray);
		 
		//Act
		 int output = matrix.AddOddElements();
		 
		//Assert
		 Assert.assertEquals(output,14);
		 
	}
	
	@Test
	public void AddEvenElements_RandomMatrix2(){
		//Arrange
		 IMatrix matrix = new Matrix();
		 int inputArray[] = {5,4,6,8,1,9,0,23,2};
		 matrix.setMatrix(inputArray);
		 
		//Act
		 int output = matrix.AddEvenElements();
		 
		//Assert
		 Assert.assertEquals(output,44);
		 
	}
	
}
