
public class Matrix implements IMatrix{
	private int matrix[][];
	
	public int[][] getMatrix(){
		return matrix;
	}
	
	public void setMatrix(int array[]){
		matrix = new int[3][3];
		int k=0;
		for (int i=0;i<matrix.length;i++){
			for(int j=0;j<matrix[i].length;j++,k++)
				this.matrix[i][j] = array[k];
		}
	}
	
	public int AddOddElements() {
		return (matrix[0][0]+matrix[0][2]+matrix[1][1]+matrix[2][0]+matrix[2][2]);
	}

	public int AddEvenElements() {
		return (matrix[0][1]+matrix[1][0]+matrix[1][2]+matrix[2][1]);
	}
}
