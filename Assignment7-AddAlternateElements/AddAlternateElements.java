import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class AddAlternateElements {
	 private static int[] getInputs() throws IOException{
		 BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		 
		 int array[] = new int[9];
		 String input[] = bufferedReader.readLine().split(" ");
		 for(int i=0;i<input.length;i++){
			 array[i] = Integer.parseInt(input[i]);
		 }
		 
		 return array;
	 }
	
	 public static void main(String[] args) throws IOException {
		 IMatrix matrix = new Matrix();
		 matrix.setMatrix(getInputs());
		 System.out.println(matrix.AddOddElements());
		 System.out.println(matrix.AddEvenElements());
	 }

}
