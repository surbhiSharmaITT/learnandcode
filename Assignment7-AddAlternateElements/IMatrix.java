
public interface IMatrix {
	int[][] getMatrix();
	void setMatrix(int array[]);
	int AddOddElements();
	int AddEvenElements();
}
