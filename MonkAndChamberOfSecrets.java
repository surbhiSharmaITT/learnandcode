import java.util.LinkedList;
import java.util.Queue;
import java.io.*;
 
class MonkAndChamberOfSecrets
 {
    Queue<Spider> spiderQueue = new LinkedList<Spider>();
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    int spidersInQueue, spidersToSelect, spidersToVisit;
    Spider temporaryArray[];
	
    public static void main(String[] args) throws Exception
    {
        MonkAndChamberOfSecrets instance = new MonkAndChamberOfSecrets();
        instance.MonkHelpToSaveHarry();
    }
	
    public void MonkHelpToSaveHarry() throws Exception
    {
	GetValidSpidersInQueueAndToSelect();
	GetValidSpiderPowers();
        temporaryArray = new Spider[spidersToSelect];
        
        for (int counter1 = 0; counter1 < spidersToSelect; counter1++)
        {
            GetSpidersToVisit();
            GetSpidersDequeuedToTemporaryArray();
            
            int indexWithMaxPower = PrintIndexWithMaxPower();
            
            for (int counter2 = 0; counter2 < spidersToVisit; counter2++)
            {
                if (counter2 != indexWithMaxPower)
                {
                    GetDecrementIndex(counter2);
                    spiderQueue.add(temporaryArray[counter2]);
                }
            }
        }
    }
	
    public void GetDecrementIndex(int index)
    {
        if(temporaryArray[index].GetPower() > 0)
        {
            temporaryArray[index].SetPower(temporaryArray[index].GetPower() - 1);
        }
    }
    
    public void GetSpidersDequeuedToTemporaryArray()
    {
        for (int counter1 = 0; counter1 < spidersToVisit; counter1++)
        {
            temporaryArray[counter1] = spiderQueue.poll();
        }
    }
    
    public void GetSpidersToVisit()
    {
	spidersToVisit = spidersToSelect;
	if (spiderQueue.size() < spidersToVisit)
        {
            spidersToVisit = spiderQueue.size();
        }
    }
	
    public void GetValidSpidersInQueueAndToSelect() throws Exception
    {
        String input[] = br.readLine().split(" ");
        spidersInQueue = Integer.parseInt(input[0]);
        spidersToSelect = Integer.parseInt(input[1]);
        
        if (!IsValidInput(spidersInQueue, spidersToSelect))
        {
            throw new Exception("Invalid Input");
        }
    }
    
    public void GetValidSpiderPowers() throws Exception
    {   
        String Input[] = br.readLine().split(" ");
        for (int i = 1; i <= spidersInQueue; i++)
        {
            spiderQueue.add(new Spider(Integer.parseInt(Input[i-1]), i));
            if(!IsValidPower(spiderQueue.element().GetPower()))
            {
                throw new Exception("Invalid Input");
            }
        }
    }

    public int PrintIndexWithMaxPower()
    {
        int max = -1, index = 0, arrayIndex = 0;
        for (int counter1 = 0; counter1 < spidersToVisit; counter1++)
        {
            if (temporaryArray[counter1].GetPower() > max)
            {
                max = temporaryArray[counter1].GetPower();
                index = temporaryArray[counter1].GetIndex();
                arrayIndex =counter1;
            }
        }
        System.out.print(index + " ");
        return arrayIndex;
    }
 
    public boolean IsValidInput(int totalSpiders, int selectSpiders)
    {
        return (totalSpiders >= selectSpiders && totalSpiders <= (selectSpiders * selectSpiders)
                && selectSpiders > 0 && selectSpiders <= 316);
    }

    public boolean IsValidPower(int spiderPower)
    {
        return (spiderPower>=1&&spiderPower<=(spidersToSelect * spidersToSelect));
    }
}
 
//Hold Spider power and its position
class Spider
{
 
    private int power;
    private int index;
 
    public Spider(int power, int index)
    {
        this.power = power;
        this.index = index;
    }

    public int GetPower()
    {
        return power;
    }
 
    public void SetPower(int power)
    {
        this.power = power;
    }
 
    public int GetIndex()
    {
        return index;
    }
}