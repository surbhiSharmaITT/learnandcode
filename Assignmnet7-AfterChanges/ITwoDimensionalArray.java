
public interface ITwoDimensionalArray {
	void LoadTwoDimArray(int array[]);
	int AddAlternateElements(int StartX, int StartY);
}
