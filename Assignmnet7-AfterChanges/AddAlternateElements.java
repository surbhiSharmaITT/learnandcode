import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class AddAlternateElements {
	 private static int[] readInputs() throws IOException{
		 BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		 
		 int array[] = new int[9];
		 String input[] = bufferedReader.readLine().split(" ");
		 for(int i=0;i<input.length;i++){
			 array[i] = Integer.parseInt(input[i]);
		 }
		 
		 return array;
	 }
	
	 public static void main(String[] args) throws IOException {
		 ITwoDimensionalArray twoDimAray = new TwoDimensionalArray();
		 twoDimAray.LoadTwoDimArray(readInputs());
		 System.out.println(twoDimAray.AddAlternateElements(0,0));
		 System.out.println(twoDimAray.AddAlternateElements(0,1));
	 }

}
