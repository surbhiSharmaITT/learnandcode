import org.junit.Assert;
import org.junit.Test;


public class AddAlternateElementsTest {
	
	@Test
	public void AddOddElements_NullTwoDimArray_Zero(){
		//Arrange
		 ITwoDimensionalArray twoDimArray = new TwoDimensionalArray();
		 int inputArray[] = {0,0,0,0,0,0,0,0,0};
		 twoDimArray.LoadTwoDimArray(inputArray);
		 
		//Act
		 int output = twoDimArray.AddAlternateElements(0,1);
		 
		//Assert
		 Assert.assertEquals(output,0);
		 
	}

	@Test
	public void AddEvenElements_NullTwoDimArray_Zero(){
		//Arrange
		 ITwoDimensionalArray twoDimArray = new TwoDimensionalArray();
		 int inputArray[] = {0,0,0,0,0,0,0,0,0};
		 twoDimArray.LoadTwoDimArray(inputArray);
		 
		//Act
		 int output = twoDimArray.AddAlternateElements(0,0);
		 
		//Assert
		 Assert.assertEquals(output,0);
		 
	}
	
	@Test
	public void AddOddElements_IdentityTwoDimensionalArray_Three(){
		//Arrange
		 ITwoDimensionalArray twoDimArray = new TwoDimensionalArray();
		 int inputArray[] = {1,0,0,0,1,0,0,0,1};
		 twoDimArray.LoadTwoDimArray(inputArray);
		 
		//Act
		 int output = twoDimArray.AddAlternateElements(0,1);
		 
		//Assert
		 Assert.assertEquals(output,3);
		 
	}

	@Test
	public void AddEvenElements_IdentityTwoDimensionalArray_Zero(){
		//Arrange
		 ITwoDimensionalArray twoDimArray = new TwoDimensionalArray();
		 int inputArray[] = {1,0,0,0,1,0,0,0,1};
		 twoDimArray.LoadTwoDimArray(inputArray);
		 
		//Act
		 int output = twoDimArray.AddAlternateElements(0,0);
		 
		//Assert
		 Assert.assertEquals(output,0);
		 
	}
	
	@Test
	public void AddOddElements_RandomTwoDimensionalArray1(){
		//Arrange
		 ITwoDimensionalArray twoDimArray = new TwoDimensionalArray();
		 int inputArray[] = {24,111,2,15,119,1,26,70,420};
		 twoDimArray.LoadTwoDimArray(inputArray);
		 
		//Act
		 int output = twoDimArray.AddAlternateElements(0,1);
		 
		//Assert
		 Assert.assertEquals(output,591);
		 
	}

	@Test
	public void AddEvenElements_RandomTwoDimensionalArray1(){
		//Arrange
		 ITwoDimensionalArray twoDimArray = new TwoDimensionalArray();
		 int inputArray[] = {24,111,2,15,119,1,26,70,420};
		 twoDimArray.LoadTwoDimArray(inputArray);
		 
		//Act
		 int output = twoDimArray.AddAlternateElements(0,0);
		 
		//Assert
		 Assert.assertEquals(output,197);
		 
	}

	@Test
	public void AddOddElements_RandomTwoDimensionalArray2(){
		//Arrange
		 ITwoDimensionalArray twoDimArray = new TwoDimensionalArray();
		 int inputArray[] = {5,4,6,8,1,9,0,23,2};
		 twoDimArray.LoadTwoDimArray(inputArray);
		 
		//Act
		 int output = twoDimArray.AddAlternateElements(0,1);
		 
		//Assert
		 Assert.assertEquals(output,14);
		 
	}
	
	@Test
	public void AddEvenElements_RandomTwoDimensionalArray2(){
		//Arrange
		 ITwoDimensionalArray twoDimArray = new TwoDimensionalArray();
		 int inputArray[] = {5,4,6,8,1,9,0,23,2};
		 twoDimArray.LoadTwoDimArray(inputArray);
		 
		//Act
		 int output = twoDimArray.AddAlternateElements(0,0);
		 
		//Assert
		 Assert.assertEquals(output,44);
		 
	}
	
}
