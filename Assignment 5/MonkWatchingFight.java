import java.io.*;

class Node 
{
    int value;
    Node leftBranch, rightBranch;
  
    Node(int value) 
    {
        this.value = value;
        leftBranch = null;
	rightBranch = null;
    }
}
  
class BinarySearchTree 
{
    protected Node root;
	 
    protected int getHeightOfBST(Node node) 
    {
        if (node == null)
            return 0;
        else
        {
            int leftSubTreeHeight = getHeightOfBST(node.leftBranch);
            int rightSubTreeHeight = getHeightOfBST(node.rightBranch);
  
            if (leftSubTreeHeight > rightSubTreeHeight)
                return (leftSubTreeHeight + 1);
            else
                return (rightSubTreeHeight + 1);
        }
    }
    
    protected void createBST(int value) 
    {
       root = addNode(root, value);
    }
     
    protected Node addNode(Node root, int value) 
    {
 	if (root == null) 
	{
            root = new Node(value);
            return root;
        }
 
        if (value <= root.value)
            root.leftBranch = addNode(root.leftBranch, value);
        else if (value > root.value)
            root.rightBranch = addNode(root.rightBranch, value);
 
        return root;
    }
	
    protected boolean isValidNumberOfElements(int numberOfElements)
    {
        return (numberOfElements>=1&&numberOfElements<=(1000));
    }
}

class MonkWatchingFight
{
    public static void main(String[] args) throws IOException
    {
        BinarySearchTree binarySearchTree = new BinarySearchTree();
		
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));  		
	int numberOfElements = Integer.parseInt(bufferedReader.readLine());
		
	if(binarySearchTree.isValidNumberOfElements(numberOfElements))
	{
	    String elements[] = bufferedReader.readLine().split(" ");
			
	    for(int elementNumber=0; elementNumber<numberOfElements; elementNumber++)
	    {
		binarySearchTree.createBST(Integer.parseInt(elements[elementNumber]));
	    }
				
	    System.out.println(binarySearchTree.getHeightOfBST(binarySearchTree.root));
	}
    }
}