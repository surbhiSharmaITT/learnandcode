import java.io.*;

class Node
{
    protected int popularity;
    protected Node link;
    public Node()
    {
        link = null;
        popularity = 0;
    }    
    public Node(int data,Node newLink)
    {
        popularity = data;
        link = newLink;
    }    
    public void SetLink(Node newLink)
    {
        link = newLink;
    }    
    public void SetPopularity(int data)
    {
        popularity = data;
    }    
    public Node GetLink()
    {
        return link;
    }    
    public int GetPopularity()
    {
        return popularity;
    }
}

class LinkedList
{
    protected Node start;
    protected Node end;
    public LinkedList()
    {
        start = null;
        end = null;
    }
    public void Append(int popularityValue)
    {
        Node newNode = new Node(popularityValue,null);    
        if(start == null) 
        {
            start = newNode;
            end = start;
        }
        else 
        {
            end.SetLink(newNode);
            end = newNode;
        }
    }
  
    public String PrintList()
    {
	String resultPart = "";
        if (start.GetLink() == null) 
        {
	    resultPart = resultPart + start.GetPopularity();
            return resultPart;
        }
		
        Node current = start;
        resultPart = resultPart + start.GetPopularity() + " ";
        current = start.GetLink();
	while (current.GetLink() != null)
        {
           resultPart = resultPart + current.GetPopularity()+" ";
           current= current.GetLink();
        }
	resultPart = resultPart + current.GetPopularity();
        return resultPart;
    }
	
    public void DeleteFriend()
    {
	boolean deleteFriend = false;
	Node current = start;
	while(current.GetLink()!= null)
	{
	   Node next = current.GetLink();
	   if(current.GetPopularity()<next.GetPopularity())
	   {
		Node tmp = next;
		int value = tmp.GetPopularity();
		current.SetLink(tmp.GetLink());
		current.SetPopularity(value);
		deleteFriend = true;
		break;
	   }
	   current = current.GetLink();
	}
        if(deleteFriend == false)
	{
	   current= start;
	   Node next = start;
	   while (current!= end)
	   {
	   	next= current;
		current= current.GetLink();
	   }
	   end = next;
	   end.SetLink(null);
	}
    }
}
public class RemoveFriends
{    
    public static void main(String[] args) throws Exception
    {    
	BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));         
        
	int noOfTestCases = Integer.parseInt(bufferedReader.readLine());
	String resultDisplayString = "";
		
        for(int testCase = 0; testCase < noOfTestCases; testCase++)
        {
            String firstLineInput[] = bufferedReader.readLine().split(" ");
	    int currentNoOfFriends = Integer.parseInt(firstLineInput[0]);
	    int friendsToDelete = Integer.parseInt(firstLineInput[1]);
	    String secondLineInput[] = bufferedReader.readLine().split(" ");
			
	    LinkedList friendList = new LinkedList();
	    for(int friendNo=0;friendNo<currentNoOfFriends;friendNo++)
	    {
		friendList.Append(Integer.parseInt(secondLineInput[friendNo]));
	    }
			
	    for(int friendNo = 0;friendNo<friendsToDelete;friendNo++)
	    {
		friendList.DeleteFriend();	
	    }
	    resultDisplayString = resultDisplayString + friendList.PrintList() + "\n";
	}  
	System.out.print(resultDisplayString);        
    }
}