import java.util.*;
import java.io.*;

class HelpMaxUsingHashMap 
{
   	HashMap <String,String> mapOfPersonNameAndTheirGame = new HashMap<>();
    	HashMap <String, Integer> mapOfGameAndItsPopularity = new HashMap<>();
	int maximumPopularity = 0;

	public void InsertingDataInMapsByMax() throws IOException
	{
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));  		
		int numberOfEntries = Integer.parseInt(bufferedReader.readLine());
		if(IsValidNumberOfEntry(numberOfEntries))
		{
	    		for(int inputEntry=0; inputEntry<numberOfEntries; inputEntry++)
	    		{
    	   			String inputLine = bufferedReader.readLine();
				String[] inputArray = inputLine.split(" ");
				if(IsValidEntryOnMap(inputArray[0] , inputArray[1]))
				{
    	   				if(!mapOfPersonNameAndTheirGame.containsKey(inputArray[0]))
    	       					mapOfPersonNameAndTheirGame.put(inputArray[0],inputArray[1]);
				}
			}
	    	}
	}

	public boolean IsValidNumberOfEntry(int numberOfEntries)
	{
		if(numberOfEntries >= 1 && numberOfEntries <= 100000)
			return true;
		else
			return false;
 	}

	public boolean IsValidEntryOnMap(String personName, String gameName)
	{
		if((personName.length() >= 1 && personName.length() <= 10) && (gameName.length() >= 1 && gameName.length() <= 10))
			return true;
		else
			return false;
 	}

	public void AppendingInMapOfGameAndItsPopularity()
	{
		for(String sport : mapOfPersonNameAndTheirGame.values())
       		{
    	   		if(!mapOfGameAndItsPopularity.containsKey(sport))
    	       			mapOfGameAndItsPopularity.put(sport,1);
    	   		else
    	       			mapOfGameAndItsPopularity.put(sport,(mapOfGameAndItsPopularity.get(sport)+1));
        	}
	}

	public String FindGameWithMaximumPopularity()
	{
		String gameWithMaximumPopularity = " ";
        	for(Map.Entry<String,Integer> sportPopularityEntry : mapOfGameAndItsPopularity.entrySet())
        	{
    	   		String sport = sportPopularityEntry.getKey();
    	   		Integer popularity = sportPopularityEntry.getValue();
    	   		if(maximumPopularity < popularity)
    	   		{
    	       			maximumPopularity = popularity;
    	       			gameWithMaximumPopularity = sport;
    	   		}
       		}
		return gameWithMaximumPopularity;
	}

	public void PrintFavoriteGame(String gameWithMaximumPopularity)
	{
		System.out.println(gameWithMaximumPopularity);
		//Evaluating the popularity of sports "Football"
    		if(mapOfGameAndItsPopularity.containsKey("football"))
    	   		System.out.println(mapOfGameAndItsPopularity.get("football"));
    		else
    	   		System.out.println("0");
	}
}
class Statistics
{
	public static void main (String args[]) throws IOException
	{
		HelpMaxUsingHashMap helpMaxUsingHashMapObject = new HelpMaxUsingHashMap();

        	helpMaxUsingHashMapObject.InsertingDataInMapsByMax();
	   	helpMaxUsingHashMapObject.AppendingInMapOfGameAndItsPopularity();
        	String gameWithMaximumPopularity = helpMaxUsingHashMapObject.FindGameWithMaximumPopularity();
		helpMaxUsingHashMapObject.PrintFavoriteGame(gameWithMaximumPopularity);
    	}
}