import org.junit.Assert;
import org.junit.Test;


public class BinarySearchTreeTest {

	@Test
    public void testGetHeightWhenRootIsNull() {
        BinarySearchTree binarySearchTree = new BinarySearchTree();
        Assert.assertEquals(binarySearchTree.getHeightOfBST(binarySearchTree.getRootNode()),0);
    }
	
	@Test
    public void testGetHeightWhenOnlyOneNode() {
        BinarySearchTree binarySearchTree = new BinarySearchTree();
        binarySearchTree.createBST(7);
        Assert.assertEquals(binarySearchTree.getHeightOfBST(binarySearchTree.getRootNode()),1);
    }
	
	@Test
    public void testGetHeightWhenTwoNodesWithLeftBranch() {
        BinarySearchTree binarySearchTree = new BinarySearchTree();
        binarySearchTree.createBST(7);
        binarySearchTree.createBST(3);
        Assert.assertEquals(binarySearchTree.getHeightOfBST(binarySearchTree.getRootNode()),2);
    }

	@Test
    public void testGetHeightWhenTwoNodesWithRightBranch() {
        BinarySearchTree binarySearchTree = new BinarySearchTree();
        binarySearchTree.createBST(7);
        binarySearchTree.createBST(10);
        Assert.assertEquals(binarySearchTree.getHeightOfBST(binarySearchTree.getRootNode()),2);
    }
	
	@Test
    public void testGetHeightWhenThreeNodesWithEqualHeight() {
        BinarySearchTree binarySearchTree = new BinarySearchTree();
        binarySearchTree.createBST(7);
        binarySearchTree.createBST(10);
        binarySearchTree.createBST(3);
        Assert.assertEquals(binarySearchTree.getHeightOfBST(binarySearchTree.getRootNode()),2);
    }
	
	@Test
    public void testGetHeightForRightTree() {
        BinarySearchTree binarySearchTree = new BinarySearchTree();
        binarySearchTree.createBST(7);
        binarySearchTree.createBST(10);
        binarySearchTree.createBST(15);
        binarySearchTree.createBST(20);
        binarySearchTree.createBST(25);
        binarySearchTree.createBST(30);
        binarySearchTree.createBST(35);
        Assert.assertEquals(binarySearchTree.getHeightOfBST(binarySearchTree.getRootNode()),7);
    }
	
	@Test
    public void testGetHeightForLeftTree() {
        BinarySearchTree binarySearchTree = new BinarySearchTree();
        binarySearchTree.createBST(70);
        binarySearchTree.createBST(65);
        binarySearchTree.createBST(60);
        binarySearchTree.createBST(20);
        binarySearchTree.createBST(15);
        binarySearchTree.createBST(10);
        Assert.assertEquals(binarySearchTree.getHeightOfBST(binarySearchTree.getRootNode()),6);
    }
	
	@Test
    public void testGetHeightForUnequalTree() {
        BinarySearchTree binarySearchTree = new BinarySearchTree();
        binarySearchTree.createBST(70);
        binarySearchTree.createBST(65);
        binarySearchTree.createBST(80);
        binarySearchTree.createBST(75);
        binarySearchTree.createBST(90);
        binarySearchTree.createBST(68);
        binarySearchTree.createBST(69);
        Assert.assertEquals(binarySearchTree.getHeightOfBST(binarySearchTree.getRootNode()),4);
    }
	
	@Test
    public void testCreateBSTWhenRootIsNull() {
        BinarySearchTree binarySearchTree = new BinarySearchTree();
        Assert.assertEquals(binarySearchTree.getRootNode(),null);
    }
	
	@Test
    public void testCreateBSTWhenOnlyOneNode() {
        BinarySearchTree binarySearchTree = new BinarySearchTree();
        binarySearchTree.createBST(7);
        Assert.assertEquals(binarySearchTree.getRootNode().value,7);
    }
	
	@Test
    public void testCreateBSTWhenTwoNodesWithLeftBranch() {
        BinarySearchTree binarySearchTree = new BinarySearchTree();
        binarySearchTree.createBST(7);
        binarySearchTree.createBST(3);
        Assert.assertEquals(binarySearchTree.getRootNode().leftBranch.value,3);
    }

	@Test
    public void testCreateBSTWhenTwoNodesWithRightBranch() {
        BinarySearchTree binarySearchTree = new BinarySearchTree();
        binarySearchTree.createBST(7);
        binarySearchTree.createBST(10);
        Assert.assertEquals(binarySearchTree.getRootNode().rightBranch.value,10);
    }
	
	@Test
    public void testCreateBSTWhenThreeNodesWithEqualHeight() {
        BinarySearchTree binarySearchTree = new BinarySearchTree();
        binarySearchTree.createBST(7);
        binarySearchTree.createBST(10);
        binarySearchTree.createBST(3);
        Assert.assertEquals(binarySearchTree.getRootNode().leftBranch.value,3);
    }
	
	@Test
    public void testCreateBSTForRightTree() {
        BinarySearchTree binarySearchTree = new BinarySearchTree();
        binarySearchTree.createBST(7);
        binarySearchTree.createBST(10);
        binarySearchTree.createBST(15);
        binarySearchTree.createBST(20);
        binarySearchTree.createBST(25);
        binarySearchTree.createBST(30);
        binarySearchTree.createBST(35);
        Assert.assertEquals(binarySearchTree.getRootNode().rightBranch.rightBranch.rightBranch.rightBranch.rightBranch.rightBranch.value,35);
    }
	
	@Test
    public void testCreateBSTForLeftTree() {
        BinarySearchTree binarySearchTree = new BinarySearchTree();
        binarySearchTree.createBST(70);
        binarySearchTree.createBST(65);
        binarySearchTree.createBST(60);
        binarySearchTree.createBST(20);
        binarySearchTree.createBST(15);
        binarySearchTree.createBST(10);
        Assert.assertEquals(binarySearchTree.getRootNode().leftBranch.leftBranch.leftBranch.leftBranch.leftBranch.value,10);
    }
	
	@Test
    public void testCreateBSTForUnequalTree() {
        BinarySearchTree binarySearchTree = new BinarySearchTree();
        binarySearchTree.createBST(70);
        binarySearchTree.createBST(65);
        binarySearchTree.createBST(80);
        binarySearchTree.createBST(75);
        binarySearchTree.createBST(90);
        binarySearchTree.createBST(68);
        binarySearchTree.createBST(69);
        Assert.assertEquals(binarySearchTree.getRootNode().leftBranch.rightBranch.rightBranch.value,69);
    }
	
	@Test
    public void testIsValidNumberOfElementsWithNumberInRange() {
        BinarySearchTree binarySearchTree = new BinarySearchTree();
        Assert.assertEquals(binarySearchTree.isValidNumberOfElements(50),true);
    }
	
	@Test
    public void testIsValidNumberOfElementsWithNumberAtLowerBoundary() {
        BinarySearchTree binarySearchTree = new BinarySearchTree();
        Assert.assertEquals(binarySearchTree.isValidNumberOfElements(1),true);
    }
	
	@Test
    public void testIsValidNumberOfElementsWithNumberAtUpperBoundary() {
        BinarySearchTree binarySearchTree = new BinarySearchTree();
        Assert.assertEquals(binarySearchTree.isValidNumberOfElements(1000),true);
    }
	
	@Test
    public void testIsValidNumberOfElementsWithNumberBelowRange() {
        BinarySearchTree binarySearchTree = new BinarySearchTree();
        Assert.assertEquals(binarySearchTree.isValidNumberOfElements(-2),false);
    }
	
	@Test
    public void testIsValidNumberOfElementsWithNumberAboveRange() {
        BinarySearchTree binarySearchTree = new BinarySearchTree();
        Assert.assertEquals(binarySearchTree.isValidNumberOfElements(1001),false);
    }
	
	@Test
    public void testGetRootNodeWithBSTTree() {
        BinarySearchTree binarySearchTree = new BinarySearchTree();
        binarySearchTree.createBST(70);
        binarySearchTree.createBST(65);
        binarySearchTree.createBST(80);
        binarySearchTree.createBST(75);
        binarySearchTree.createBST(90);
        binarySearchTree.createBST(68);
        binarySearchTree.createBST(69);
        Assert.assertEquals(binarySearchTree.getRootNode().value,70);
    }
	
	@Test
    public void testGetRootNodeWhenRootIsNull() {
        BinarySearchTree binarySearchTree = new BinarySearchTree();
        Assert.assertEquals(binarySearchTree.getRootNode(),null);
    }
}