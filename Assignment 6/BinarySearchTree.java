
public class BinarySearchTree {
    private Node root;
	
    public Node getRootNode() {
	    return root;
	}
	
    public int getHeightOfBST(Node node) {
        if (node == null)
            return 0;
        else {
            int leftSubTreeHeight = getHeightOfBST(node.leftBranch);
            int rightSubTreeHeight = getHeightOfBST(node.rightBranch);
  
            if (leftSubTreeHeight > rightSubTreeHeight)
                return (leftSubTreeHeight + 1);
            else
                return (rightSubTreeHeight + 1);
        }
    }
    
    public void createBST(int value) {
       root = addNode(root, value);
    }
     
    private Node addNode(Node root, int value) {
    	if (root == null) {
            root = new Node(value);
            return root;
        }
 
        if (value <= root.value)
            root.leftBranch = addNode(root.leftBranch, value);
        else if (value > root.value)
            root.rightBranch = addNode(root.rightBranch, value);
 
        return root;
    }
	
    public boolean isValidNumberOfElements(int numberOfElements){
        return (numberOfElements>=1&&numberOfElements<=(1000));
    }
}
