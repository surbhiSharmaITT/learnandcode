import java.io.*;
  
public class Assignment5 {
    public static void main(String[] args) throws IOException {
        BinarySearchTree binarySearchTree = new BinarySearchTree();
		
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));  		
        int numberOfElements = Integer.parseInt(bufferedReader.readLine());
		
        if(binarySearchTree.isValidNumberOfElements(numberOfElements)) {
        	String elements[] = bufferedReader.readLine().split(" ");
			
        	for(int elementNumber=0; elementNumber<numberOfElements; elementNumber++){
        		binarySearchTree.createBST(Integer.parseInt(elements[elementNumber]));
        	}
				
        	System.out.println(binarySearchTree.getHeightOfBST(binarySearchTree.getRootNode()));
        }
    }
}
