class Node {
    int value;
    Node leftBranch, rightBranch;
    
    Node(int value) {
        this.value = value;
        leftBranch = null;
        rightBranch = null;
    }
}
