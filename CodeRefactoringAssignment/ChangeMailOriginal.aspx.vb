﻿Imports System.Data

Partial Class ChangeMail
    Inherits System.Web.UI.Page
    Dim loggedInUser As MembershipUser

    Protected Function isEmailChangedByAdmin(userID As Guid) As Boolean
        Dim connectionString As String = ConfigurationManager.ConnectionStrings("LocalSqlServer").ConnectionString
        Dim myConnection As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(connectionString)
        Dim selectSql As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand("select isEmailChangedByAdmin from [dbo].[aspnet_Membership] where UserID ='" + userID.ToString + "'", myConnection)
        Dim isEmailChanged As Boolean = False
        Dim drFoundRec As System.Data.SqlClient.SqlDataReader

        myConnection.Open()
        drFoundRec = selectSql.ExecuteReader()

        If drFoundRec.HasRows Then
            drFoundRec.Read()
            isEmailChanged = drFoundRec(0)
        End If

        selectSql.Dispose()
        drFoundRec.Dispose()
        myConnection.Close()

        Return isEmailChanged
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim beforeProcess As Long = System.Diagnostics.Process.GetCurrentProcess().WorkingSet64
        Dim redirect As String
        redirect = System.Web.HttpUtility.UrlDecode(Request.QueryString.Get("redirect"))
        loggedInUser = Membership.GetUser()
        If Not String.IsNullOrEmpty(redirect) Then
            LblExistingUserWithEmail.Visible = False
            LblExistingUserWithoutEmail.Visible = False
            LblAdminChangedEmail.Visible = False
            LblUsingDifferentEmail.Visible = False
            TxtEmail.Enabled = True
            BtnDifferentEmail.Visible = False
            LblConfirmEmail.Visible = True
            TxtConfirmEmail.Visible = True
            YouAreLoggedIn.Text = "<span class='col-md-8  col-md-offset-2 marginBottom10 bg-danger userLoggedIn' style='color:#752F8B;' >User """ + Membership.GetUser().UserName + """ is logged in.</span>"
            YouAreLoggedIn.Visible = True
            GoToHome.Visible = True
            LoggedIn.Visible = True
            SignOut.Visible = True
        Else
            If Not String.IsNullOrEmpty(loggedInUser.Email) Then
                LblExistingUserWithEmail.Visible = True
                LblExistingUserWithoutEmail.Visible = False
                LblAdminChangedEmail.Visible = False
                LblUsingDifferentEmail.Visible = False
                BtnDifferentEmail.Visible = True
                YouAreLoggedIn.Visible = False
                GoToHome.Visible = False
                LoggedIn.Visible = False
                SignOut.Visible = True
                If Not Page.IsPostBack Then
                    TxtEmail.Text = loggedInUser.Email
                End If
            Else
                LblExistingUserWithEmail.Visible = False
                LblExistingUserWithoutEmail.Visible = True
                LblAdminChangedEmail.Visible = False
                LblUsingDifferentEmail.Visible = False
                TxtEmail.Enabled = True
                BtnDifferentEmail.Visible = False
                LblConfirmEmail.Visible = True
                TxtConfirmEmail.Visible = True
                YouAreLoggedIn.Visible = False
                GoToHome.Visible = False
                LoggedIn.Visible = False
                SignOut.Visible = True
            End If
        End If

        If isEmailChangedByAdmin(loggedInUser.ProviderUserKey) Then
            LblExistingUserWithEmail.Visible = False
            LblExistingUserWithoutEmail.Visible = False
            LblAdminChangedEmail.Visible = True
            LblUsingDifferentEmail.Visible = False
        End If

        Dim afterProcess As Long = System.Diagnostics.Process.GetCurrentProcess().WorkingSet64
        Dim fileHandler As New FileHandling()
        fileHandler.AppendMemoryLog(beforeProcess, afterProcess, "ChangeMail- Page Load")
        fileHandler = Nothing
    End Sub

    Protected Sub BtnAcceptEmail_Click(sender As Object, e As EventArgs) Handles BtnAcceptEmail.Click
        loggedInUser = Membership.GetUser()
        Dim userId As String = loggedInUser.ProviderUserKey.ToString().ToUpper()
        Dim oldUsername As String = loggedInUser.UserName
        Dim oldEmail As String = loggedInUser.Email
        Dim isSetUsernameAsEmailSuccessful As Integer

        Dim connectionString As String = ConfigurationManager.ConnectionStrings("LocalSqlServer").ConnectionString
        Dim myConnection As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(connectionString)
        myConnection.Open()

        If TxtEmail.Enabled = True Then
            'for the case when email is not retrieved from database i.e entered by the user
            Dim newEmail As String = TxtEmail.Text
            Dim confirmedEmail As String = TxtConfirmEmail.Text
            Dim updateUsernameSql As String = "UPDATE [dbo].[aspnet_Users] SET UserName = @email, LoweredUserName = LOWER(@email) WHERE UserId = @userId "
            Dim updateUserNameCommand As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand(updateUsernameSql, myConnection)
            updateUserNameCommand.Parameters.AddWithValue("@userId", userId)
            updateUserNameCommand.Parameters.AddWithValue("@email", newEmail)
            Try
                isSetUsernameAsEmailSuccessful = updateUserNameCommand.ExecuteNonQuery()
                updateUserNameCommand.Dispose()

                If isSetUsernameAsEmailSuccessful = 1 Then
                    'if username changed to email successfully
                    'change old email to the new email, change web admin mapping and set isEmailVerified = 1
                    Dim updateMembershipTableSql As String = "UPDATE [dbo].[aspnet_Membership] SET Email = @email, LoweredEmail = LOWER(@email), isEmailVerified = 1 WHERE UserId = @userId  "
                    Dim updateMembershipTableCommand As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand(updateMembershipTableSql, myConnection)
                    updateMembershipTableCommand.Parameters.AddWithValue("@userId", userId)
                    updateMembershipTableCommand.Parameters.AddWithValue("@email", newEmail)
                    Dim updateWebAdminMappingSql As String = "UPDATE [user].[WebAdminBindings] SET UserName = @email WHERE Username = @username "
                    Dim updateWebAdminMappingCommand As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand(updateWebAdminMappingSql, myConnection)
                    updateWebAdminMappingCommand.Parameters.AddWithValue("@username", oldUsername)
                    updateWebAdminMappingCommand.Parameters.AddWithValue("@email", newEmail)
                    Try
                        If updateMembershipTableCommand.ExecuteNonQuery() <> 0 And updateWebAdminMappingCommand.ExecuteNonQuery() <> 0 Then
                            updateMembershipTableCommand.Dispose()
                            updateWebAdminMappingCommand.Dispose()
                            Dim updatedUser As MembershipUser = Membership.GetUser(Guid.Parse(userId))
                            Membership.UpdateUser(updatedUser)
                            FormsAuthentication.SetAuthCookie(newEmail, False)
                            If isEmailChangedByAdmin(loggedInUser.ProviderUserKey) Then
                                Dim updateAdminChangesSql As String = "UPDATE [dbo].[aspnet_Membership] SET isEmailChangedByAdmin = 0 WHERE UserId = @userId  "
                                Dim updateAdminChangesCommand As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand(updateAdminChangesSql, myConnection)
                                updateAdminChangesCommand.Parameters.AddWithValue("@userId", userId)
                                updateAdminChangesCommand.ExecuteNonQuery()
                                updateAdminChangesCommand.Dispose()
                            End If
                            Dim redirect As String = System.Web.HttpUtility.UrlDecode(Request.QueryString.Get("redirect"))
                            If Not String.IsNullOrEmpty(redirect) Then
                                Dim mailText As String = "You have requested a change in email address in your IdahoSTARS Provider Account. Use this email address to login next time: "
                                mailText += newEmail
                                MMod.SendEMailNow(newEmail, "Email Address Changed", mailText)
                            End If
                            If myConnection.State = ConnectionState.Open Then
                                myConnection.Close()
                            End If
                            Response.Redirect("default.aspx", False)
                        Else
                            updateMembershipTableCommand.Dispose()
                            updateWebAdminMappingCommand.Dispose()
                            Throw New System.Exception
                        End If
                    Catch ex As Exception
                        Dim revertUsernameSql As String = "UPDATE [dbo].[aspnet_Users] SET UserName = @oldUserName, LoweredUserName = LOWER(@oldUserName) WHERE UserId = @userId "
                        Dim revertUserNameCommand As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand(revertUsernameSql, myConnection)
                        revertUserNameCommand.Parameters.AddWithValue("@userId", userId)
                        revertUserNameCommand.Parameters.AddWithValue("@oldUserName", oldUsername)
                        revertUserNameCommand.ExecuteNonQuery()
                        revertUserNameCommand.Dispose()
                        If myConnection.State = ConnectionState.Open Then
                            myConnection.Close()
                        End If
                        Session.Abandon()
                        FormsAuthentication.SignOut()
                        Response.Redirect("~/Login.aspx?message=Sorry, Email cannot be verified, please try again. If you receive an error twice, please contact the IdahoSTARS Training Office for assistance.")
                    End Try
                End If
            Catch ex As Exception
                'when same username already exist in the database
                If isSetUsernameAsEmailSuccessful = 0 Then
                    Dim selectLinkedUserSql As String = "SELECT (ISNULL([first_name], ' ') + ' ' + ISNULL([last_name],' ')) as full_name FROM aspnet_membership, [user].[profile] WHERE [user].[profile].userid = aspnet_membership.userid and loweredemail = @email"
                    Dim selectLinkedUserCommand As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand(selectLinkedUserSql, myConnection)
                    selectLinkedUserCommand.Parameters.AddWithValue("@email", TxtEmail.Text)
                    Dim drFoundRec As System.Data.SqlClient.SqlDataReader
                    drFoundRec = selectLinkedUserCommand.ExecuteReader()

                    While drFoundRec.Read
                        Dim userFullName As String = drFoundRec(0).ToString
                        Dim redirect As String = System.Web.HttpUtility.UrlDecode(Request.QueryString.Get("redirect"))
                        If Not String.IsNullOrEmpty(redirect) Then
                            ErrorMessage.Text = "This email address is already in use."
                            ErrorMessage.Visible = True
                        Else
                            If myConnection.State = ConnectionState.Open Then
                                myConnection.Close()
                            End If
                            Session.Abandon()
                            FormsAuthentication.SignOut()
                            Response.Redirect("~/Login.aspx?message=The email address is already in use for " + userFullName + ". Please reset your password or contact the IdahoSTARS Training Office for assistance.")
                        End If
                    End While
                    drFoundRec.Dispose()
                    selectLinkedUserCommand.Dispose()
                    If myConnection.State = ConnectionState.Open Then
                        myConnection.Close()
                    End If
                End If
            End Try
        Else
            'for the case when email is retrieved from the database
            Dim updateUsernameSql As String = "UPDATE [dbo].[aspnet_Users] SET UserName = @email, LoweredUserName = LOWER(@email) WHERE UserId = @userId "
            Dim updateUserNameCommand As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand(updateUsernameSql, myConnection)
            updateUserNameCommand.Parameters.AddWithValue("@userId", userId)
            updateUserNameCommand.Parameters.AddWithValue("@email", oldEmail)
            Try
                isSetUsernameAsEmailSuccessful = updateUserNameCommand.ExecuteNonQuery()
                If isSetUsernameAsEmailSuccessful = 1 Then
                    'if username changed to email successfully
                    'change web admin mapping and set isEmailVerified = 1
                    Dim updateMembershipTableSql As String = "UPDATE [dbo].[aspnet_Membership] SET isEmailVerified = 1 WHERE UserId = @userId  "
                    Dim updateMembershipTableCommand As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand(updateMembershipTableSql, myConnection)
                    updateMembershipTableCommand.Parameters.AddWithValue("@userId", userId)
                    Dim updateWebAdminMappingSql As String = "UPDATE [user].[WebAdminBindings] SET UserName = @email WHERE Username = @username "
                    Dim updateWebAdminMappingCommand As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand(updateWebAdminMappingSql, myConnection)
                    updateWebAdminMappingCommand.Parameters.AddWithValue("@username", oldUsername)
                    updateWebAdminMappingCommand.Parameters.AddWithValue("@email", oldEmail)
                    Try
                        If updateMembershipTableCommand.ExecuteNonQuery() <> 0 And updateWebAdminMappingCommand.ExecuteNonQuery() <> 0 Then
                            Dim updatedUser As MembershipUser = Membership.GetUser(Guid.Parse(userId))
                            Membership.UpdateUser(updatedUser)
                            FormsAuthentication.SetAuthCookie(oldEmail, False)
                            If isEmailChangedByAdmin(loggedInUser.ProviderUserKey) Then
                                Dim updateAdminChangesSql As String = "UPDATE [dbo].[aspnet_Membership] SET isEmailChangedByAdmin = 0 WHERE UserId = @userId  "
                                Dim updateAdminChangesCommand As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand(updateAdminChangesSql, myConnection)
                                updateAdminChangesCommand.Parameters.AddWithValue("@userId", userId)
                                updateAdminChangesCommand.ExecuteNonQuery()
                                updateAdminChangesCommand.Dispose()
                            End If
                            updateMembershipTableCommand.Dispose()
                            updateWebAdminMappingCommand.Dispose()
                            If myConnection.State = ConnectionState.Open Then
                                myConnection.Close()
                            End If
                            Response.Redirect("default.aspx", False)
                        Else
                            updateMembershipTableCommand.Dispose()
                            updateWebAdminMappingCommand.Dispose()
                            Throw New System.Exception
                        End If
                    Catch ex As Exception
                        Dim revertUsernameSql As String = "UPDATE [dbo].[aspnet_Users] SET UserName = @oldUserName, LoweredUserName = LOWER(@oldUserName) WHERE UserId = @userId "
                        Dim revertUserNameCommand As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand(revertUsernameSql, myConnection)
                        revertUserNameCommand.Parameters.AddWithValue("@userId", userId)
                        revertUserNameCommand.Parameters.AddWithValue("@oldUserName", oldUsername)
                        revertUserNameCommand.ExecuteNonQuery()
                        revertUserNameCommand.Dispose()
                        If myConnection.State = ConnectionState.Open Then
                            myConnection.Close()
                        End If
                        Session.Abandon()
                        FormsAuthentication.SignOut()
                        Response.Redirect("~/Login.aspx?message=Sorry, Email cannot be verified, please try again. If you receive an error twice, please contact the IdahoSTARS Training Office for assistance.")
                    End Try
                End If
            Catch ex As Exception
                'when same username already exist in the database
                If isSetUsernameAsEmailSuccessful = 0 Then
                    Dim selectLinkedUserSql As String = "SELECT (ISNULL([first_name], ' ') + ' ' + ISNULL([last_name],' ')) as full_name FROM aspnet_membership, [user].[profile] WHERE [user].[profile].userid = aspnet_membership.userid and loweredemail = @email"
                    Dim selectLinkedUserCommand As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand(selectLinkedUserSql, myConnection)
                    selectLinkedUserCommand.Parameters.AddWithValue("@email", TxtEmail.Text)
                    Dim drFoundRec As System.Data.SqlClient.SqlDataReader
                    drFoundRec = selectLinkedUserCommand.ExecuteReader()

                    While drFoundRec.Read
                        Dim userFullName As String = drFoundRec(0).ToString
                        If myConnection.State = ConnectionState.Open Then
                            myConnection.Close()
                        End If
                        Session.Abandon()
                        FormsAuthentication.SignOut()
                        Response.Redirect("~/Login.aspx?message=The email address is already in use for " + userFullName + ". Please reset your password or contact the IdahoSTARS Training Office for assistance.")
                    End While
                    drFoundRec.Dispose()
                    selectLinkedUserCommand.Dispose()
                End If
            End Try
            updateUserNameCommand.Dispose()
        End If
        If myConnection.State = ConnectionState.Open Then
            myConnection.Close()
        End If
    End Sub

    Protected Sub BtnDifferentEmail_Click(sender As Object, e As EventArgs) Handles BtnDifferentEmail.Click
        TxtEmail.Text = String.Empty
        TxtEmail.Enabled = True
        LblConfirmEmail.Visible = True
        TxtConfirmEmail.Visible = True
        BtnDifferentEmail.Visible = False
        LblExistingUserWithoutEmail.Visible = True
        LblExistingUserWithEmail.Visible = False
        If isEmailChangedByAdmin(loggedInUser.ProviderUserKey) Then
            LblExistingUserWithoutEmail.Visible = False
            LblAdminChangedEmail.Visible = False
            LblUsingDifferentEmail.Visible = True
        End If
    End Sub

    Protected Sub GoToHome_Click1(sender As Object, e As EventArgs) Handles GoToHome.Click
        Session("ViewState") = Nothing
        Response.Redirect("~/default.aspx")
    End Sub

    Protected Sub SignOut_Click(sender As Object, e As EventArgs) Handles SignOut.Click
        Session("ViewState") = Nothing
        Response.Redirect("~/Login.aspx?redirect=Logout")
    End Sub
End Class
