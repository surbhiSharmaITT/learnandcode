﻿Imports System.Data

Partial Class ChangeMail
    Inherits System.Web.UI.Page
    Dim LoggedInUser As MembershipUser
    Dim ConnectionString As String = ConfigurationManager.ConnectionStrings("LocalSqlServer").ConnectionString

#Region "Constants"
    Public Const DEFAULTREDIRECTINGURL As String = "~/default.aspx"
    Public Const LOGOUTREDIRECTINGURL As String = "~/Login.aspx?redirect=Logout"
    Public Const LOGINREDIRECTINGURLWITHMESSAGE As String = "~/Login.aspx?message=The email address is already in use for {0}. Please reset your password or contact the IdahoSTARS Training Office for assistance."
    Public Const ERROREMAILALREADYEXISTS As String = "This email address is already in use."
    Public Const EMAILTEXT As String = "You have requested a change in email address in your IdahoSTARS Provider Account. Use this email address to login next time: "
    Public Const ISEMAILCHANGEDBYADMINSQL As String = "select isEmailChangedByAdmin from [dbo].[aspnet_Membership] where UserID ='{0}'"
    Public Const UPDATEUSERNAMETOEMAILSQL As String = "UpdateUsernameToEmail"
    Public Const SELECTLINKEDUSERSQL As String = "SELECT (ISNULL([first_name], ' ') + ' ' + ISNULL([last_name],' ')) as full_name FROM aspnet_membership, [user].[profile] WHERE [user].[profile].userid = aspnet_membership.userid and lower(Email) = lower(@email)"
#End Region

#Region "Page Initialization and Modifications"
    Private Sub ChangeHeading(ByVal lblHeadingUpdateInfoVisibility As Boolean, ByVal lblHeadingAdminUpdateInfoVisibility As Boolean, ByVal lblHeadingConfirmEmailInfoVisibility As Boolean, ByVal lblHeadingEnterEmailInfoVisibility As Boolean)
        LblHeadingUpdateInfo.Visible = lblHeadingUpdateInfoVisibility
        LblHeadingAdminUpdateInfo.Visible = lblHeadingAdminUpdateInfoVisibility
        LblHeadingConfirmEmailInfo.Visible = lblHeadingConfirmEmailInfoVisibility
        LblHeadingEnterEmailInfo.Visible = lblHeadingEnterEmailInfoVisibility
    End Sub
    Private Sub AddConfirmEmailField()
        LblConfirmEmail.Visible = True
        TxtConfirmEmail.Visible = True
        BtnDifferentEmail.Visible = False
        TxtEmail.Enabled = True
    End Sub
    Private Sub RemoveConfirmEmailField()
        LblConfirmEmail.Visible = False
        TxtConfirmEmail.Visible = False
        BtnDifferentEmail.Visible = True
    End Sub
    Private Sub AddOptions(ByVal goToHomeVisibility As Boolean, ByVal signOutVisibility As Boolean, ByVal loggedInVisibility As Boolean, ByVal youAreLoggedInVisibility As Boolean)
        GoToHome.Visible = goToHomeVisibility
        SignOut.Visible = signOutVisibility
        LoggedIn.Visible = loggedInVisibility
        YouAreLoggedIn.Visible = youAreLoggedInVisibility
    End Sub
#End Region

#Region "DBMS Functionality"
    Private Function IsEmailChangedByAdmin(userID As Guid) As Boolean
        Dim isEmailChanged As Boolean = False
        Using myConnection As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(ConnectionString)
            Using selectSql As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand(String.Format(ISEMAILCHANGEDBYADMINSQL, userID.ToString()), myConnection)
                myConnection.Open()
                Using drFoundRec As System.Data.SqlClient.SqlDataReader = selectSql.ExecuteReader()
                    If drFoundRec.HasRows Then
                        drFoundRec.Read()
                        isEmailChanged = drFoundRec(0)
                    End If
                End Using
                If myConnection.State = ConnectionState.Open Then
                    myConnection.Close()
                End If
            End Using
        End Using
        Return isEmailChanged
    End Function
    Private Function ExecuteUpdateUserNameQuery(ByVal email As String, ByVal username As String, ByVal userId As String, ByVal isEmailChangedByAdmin As Boolean) As Integer
        Dim hasSetUsernameAsEmailSuccessful As Integer
        Using myConnection As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(ConnectionString)
            myConnection.Open()
            Using updateUserNameSql As System.Data.SqlClient.SqlCommand = myConnection.CreateCommand()
                updateUserNameSql.CommandType = CommandType.StoredProcedure
                updateUserNameSql.CommandText = UPDATEUSERNAMETOEMAILSQL
                updateUserNameSql.Parameters.AddWithValue("@email", email)
                updateUserNameSql.Parameters.AddWithValue("@username", username)
                updateUserNameSql.Parameters.AddWithValue("@userId", userId)
                updateUserNameSql.Parameters.AddWithValue("@isEmailChangedByAdmin", isEmailChangedByAdmin)
                Try
                    hasSetUsernameAsEmailSuccessful = updateUserNameSql.ExecuteNonQuery()
                Catch ex As Exception
                    hasSetUsernameAsEmailSuccessful = 0
                End Try
            End Using
            If myConnection.State = ConnectionState.Open Then
                myConnection.Close()
            End If
        End Using
        Return hasSetUsernameAsEmailSuccessful
    End Function
    Private Function GetLinkedUser(ByVal email As String) As String
        Dim userFullName As String = String.Empty
        Using myConnection As System.Data.SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(ConnectionString)
            myConnection.Open()
            Using selectLinkedUserCommand As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand(SELECTLINKEDUSERSQL, myConnection)
                selectLinkedUserCommand.Parameters.AddWithValue("@email", email)
                Using drFoundRec As System.Data.SqlClient.SqlDataReader = selectLinkedUserCommand.ExecuteReader()
                    If drFoundRec.HasRows Then
                        drFoundRec.Read()
                        userFullName = drFoundRec(0).ToString
                    End If
                    drFoundRec.Close()
                End Using
            End Using
            If myConnection.State = ConnectionState.Open Then
                myConnection.Close()
            End If
        End Using
        Return userFullName
    End Function
#End Region

#Region "Business Logics"
    Private Sub UpdateUser(ByVal userId As String, ByVal email As String)
        Dim updatedUser As MembershipUser = Membership.GetUser(Guid.Parse(userId))
        Membership.UpdateUser(updatedUser)
        FormsAuthentication.SetAuthCookie(email, False)
    End Sub
    Private Sub DisplayErrorAsEmailAlreadyUsed()
        Dim userFullName As String = GetLinkedUser(TxtEmail.Text)
        Session.Abandon()
        FormsAuthentication.SignOut()
        Response.Redirect(String.Format(LOGINREDIRECTINGURLWITHMESSAGE, userFullName))
    End Sub
    Public Function HasRedirectValueInQueryString() As Boolean
        Dim redirectValue As String = System.Web.HttpUtility.UrlDecode(Request.QueryString.Get("redirect"))
        If String.IsNullOrEmpty(redirectValue) Then
            Return False
        Else
            Return True
        End If
    End Function
#End Region

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        LoggedInUser = Membership.GetUser()
        If HasRedirectValueInQueryString() Then
            ChangeHeading(False, False, False, False)
            AddConfirmEmailField()
            AddOptions(True, True, True, True)
            YouAreLoggedIn.Text = "<span class='col-md-8  col-md-offset-2 marginBottom10 bg-danger userLoggedIn' style='color:#752F8B;' >User """ + Membership.GetUser().UserName + """ is logged in.</span>"
        Else
            If Not String.IsNullOrEmpty(LoggedInUser.Email) Then
                ChangeHeading(True, False, True, False)
                RemoveConfirmEmailField()
                AddOptions(False, True, False, False)
                If Not Page.IsPostBack Then
                    TxtEmail.Text = LoggedInUser.Email
                End If
            Else
                ChangeHeading(True, False, False, True)
                AddConfirmEmailField()
                AddOptions(False, True, False, False)
            End If
        End If
        If IsEmailChangedByAdmin(LoggedInUser.ProviderUserKey) Then
            ChangeHeading(False, True, True, False)
        End If
    End Sub
    Protected Sub BtnAcceptEmail_Click(sender As Object, e As EventArgs) Handles BtnAcceptEmail.Click
        LoggedInUser = Membership.GetUser()
        Dim userId As String = LoggedInUser.ProviderUserKey.ToString().ToUpper()
        Dim oldUsername As String = LoggedInUser.UserName
        Dim oldEmail As String = LoggedInUser.Email
        Dim isSetUsernameAsEmailSuccessful As Integer
        If TxtEmail.Enabled = True Then
            'for the case when email is not retrieved from database i.e entered by the user
            Dim newEmail As String = TxtEmail.Text
            isSetUsernameAsEmailSuccessful = ExecuteUpdateUserNameQuery(newEmail, oldUsername, userId, IsEmailChangedByAdmin(LoggedInUser.ProviderUserKey))
            If isSetUsernameAsEmailSuccessful Then
                UpdateUser(userId, newEmail)
                If HasRedirectValueInQueryString() Then
                    Dim mailText As String = EMAILTEXT
                    mailText += newEmail
                    MMod.SendEMailNow(newEmail, "Email Address Changed", mailText)
                End If
                Response.Redirect(DEFAULTREDIRECTINGURL, False)
            Else
                If HasRedirectValueInQueryString() Then
                    ErrorMessage.Text = ERROREMAILALREADYEXISTS
                    ErrorMessage.Visible = True
                Else
                    DisplayErrorAsEmailAlreadyUsed()
                End If
            End If
        Else
            'for the case when email is retrieved from the database
            isSetUsernameAsEmailSuccessful = ExecuteUpdateUserNameQuery(oldEmail, oldUsername, userId, IsEmailChangedByAdmin(LoggedInUser.ProviderUserKey))
            If isSetUsernameAsEmailSuccessful Then
                UpdateUser(userId, oldEmail)
                Response.Redirect(DEFAULTREDIRECTINGURL, False)
            Else
                DisplayErrorAsEmailAlreadyUsed()
            End If
        End If
    End Sub
    Protected Sub BtnDifferentEmail_Click(sender As Object, e As EventArgs) Handles BtnDifferentEmail.Click
        TxtEmail.Text = String.Empty
        AddConfirmEmailField()
        ChangeHeading(True, False, False, True)
        If IsEmailChangedByAdmin(LoggedInUser.ProviderUserKey) Then
            ChangeHeading(False, True, False, True)
        End If
    End Sub
    Protected Sub GoToHome_Click1(sender As Object, e As EventArgs) Handles GoToHome.Click
        Session("ViewState") = Nothing
        Response.Redirect(DEFAULTREDIRECTINGURL)
    End Sub
    Protected Sub SignOut_Click(sender As Object, e As EventArgs) Handles SignOut.Click
        Session("ViewState") = Nothing
        Response.Redirect(LOGOUTREDIRECTINGURL)
    End Sub
#End Region
End Class